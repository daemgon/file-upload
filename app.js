const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

const PORT = 8000;

// express.static(root, [options])
//app.use('/static', express.static(__dirname + '/index.html'));

// app.use(express.static('public'))

// create a virtual path prefix (where the path 
// to access http://localhost:8000/static/image.png
app.use('/static', express.static(__dirname + '/public'));
const fileuUploadConfig = {
    imits: { fileSize: 50 * 1024 * 1024 }
}
// default options
app.use(fileUpload(fileuUploadConfig));

app.get('/ping', function(req, res) {
  res.send('pong');
});

app.post('/upload', function(req, res) {
  let inputFile;
  let uploadPath;

  if (!req.files || Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  console.log('req.files >>>', req.files); // eslint-disable-line

  inputFile = req.files.inputFile;

  // uploadPath = __dirname + '/uploads/' + inputFile.name;
  uploadPath = `${__dirname}/public/${inputFile.name}`
  inputFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    //res.send('File uploaded to ' + uploadPath + ': ' + uploadPath)
    //res.json({file: `static/${req.body.inputFile}.jpg`});;
    res
        .status(200)
        .json({ 
            message: `uploaded to ${uploadPath}`,
            file: `static/${inputFile.name}`
        });
  });
});

app.listen(PORT, function() {
  console.log('Express server listening on port ', PORT); // eslint-disable-line
});